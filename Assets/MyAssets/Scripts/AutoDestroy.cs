﻿using System.Collections;
using UnityEngine;

namespace YsoCorp
{
    public class AutoDestroy : YCBehaviour
    {
        [SerializeField] private float _delay = 10;

        private void Start()
        {
            Invoke("Destroy", this._delay);
        }

        private void Destroy()
        {
            Destroy(this.gameObject);
        }
    }
}