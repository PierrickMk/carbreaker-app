﻿using System.Collections;
using UnityEngine;

namespace YsoCorp
{
    public class CarDestruction : YCBehaviour
    {
        [SerializeField] private Animator _anim;
        [SerializeField] private WheelBehaviour[] _wheels;
        [SerializeField] private Vector3 wheelVelocity;
        private Rigidbody _rb;
        private Collider[] _cols;
        private bool _isDestroyed = false;

        private void Start()
        {
            this._rb = GetComponent<Rigidbody>();
            this._cols = GetComponents<Collider>();
        }

        public void Destroy()
        {
            if (_isDestroyed == true) return;

            this._anim.SetTrigger("Destroy");
            //this._rb.isKinematic = true;
            this._rb.velocity *= 2;
            this._isDestroyed = true;

            foreach(var col in this._cols)
            {
                col.enabled = false;
            }

            for (int i = 0; i < _wheels.Length; i++)
            {
                _wheels[i].Detach();
                _wheels[i].SetVelocity(wheelVelocity);
            }

            //GameObject vfxSmoke = GamePool.Instance.GetObject("vfx_smoke");
            //GamePool.SetupObject(vfxSmoke, transform);

            GameObject vfxExplosion = GamePool.Instance.GetObject("vfx_small_explosion");
            GamePool.SetupObject(vfxExplosion, transform);
        }
    }
}