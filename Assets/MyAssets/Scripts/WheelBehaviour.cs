﻿using System.Collections;
using UnityEngine;

namespace YsoCorp
{
    public class WheelBehaviour : YCBehaviour
    {
        [SerializeField] private Vector3 _detachForce;
        [SerializeField] private Vector3 _detachTorque;
        [Header("Optional")]
        [SerializeField] private bool _randomTorque;
        [SerializeField] private float _randomTorqueMin = 0;
        [SerializeField] private float _randomTorqueMax = 10;
        private Collider _col;
        private Rigidbody _rb;

        private void Start()
        {
            this._rb = GetComponent<Rigidbody>();
            this._col = GetComponent<Collider>();
            this._rb.isKinematic = true;
            this._col.isTrigger = true;
        }

        public void Detach()
        {
            this._rb.isKinematic = false;
            this._rb.AddRelativeForce(this._detachForce);
            this._rb.AddRelativeTorque(this._detachTorque);

            if(this._randomTorque == true)
            {
                this._rb.AddRelativeTorque(RandomTorque());
            }

            this._col.isTrigger = false;
        }

        public void SetVelocity(Vector3 vel) => _rb.velocity = vel;

        private Vector3 RandomTorque()
        {
            return new Vector3(Random.Range(this._randomTorqueMin, this._randomTorqueMax), Random.Range(this._randomTorqueMin, this._randomTorqueMax), Random.Range(this._randomTorqueMin, this._randomTorqueMax));
        }
    }
}