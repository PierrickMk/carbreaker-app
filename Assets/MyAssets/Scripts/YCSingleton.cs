﻿using UnityEngine;

namespace YsoCorp
{
    public class YCSingleton<T> : YCBehaviour where T : Component
    {
        public static T Instance { get; private set; }

		protected override void Awake()
		{
			base.Awake();

			if (Instance != null)
			{
				Destroy(gameObject);
				return;
				
			}

			Instance = this as T;
		}
	}
}