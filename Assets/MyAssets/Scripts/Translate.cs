﻿using UnityEngine;

namespace YsoCorp
{
    public class Translate : YCBehaviour
    {
        [SerializeField] private Vector3 _velocity;

        private void Update()
        {
            transform.position += _velocity * Time.deltaTime;
        }
    }
}