﻿using System.Collections;
using UnityEngine;

namespace YsoCorp
{
    [RequireComponent(typeof(Rigidbody))]
    public class Mover : YCBehaviour
    {
        [SerializeField] private Vector3 _velocity;
        private Rigidbody _rb;

        void Start()
        {
            this._rb = GetComponent<Rigidbody>();
            this._rb.velocity = this._velocity;
        }
    }
}