﻿using System.Collections;
using UnityEngine;

namespace YsoCorp
{
    public class Spawner : YCBehaviour
    {
        [SerializeField] private GameObject[] prefabs;
        [SerializeField] private int[] instanceCount;
        [SerializeField] private Transform[] spawns;
        [SerializeField] private float startDelay;
        [SerializeField] private float delay;
        [SerializeField] private float stepDelay;
        [SerializeField] private float minDelay;
        private int currentSpawn = 0;
        private int currentPrefab = 0;
        private int instances;

        private void Start()
        {
            StartCoroutine(SpawnProcess());   
        }

        private IEnumerator SpawnProcess()
        {
            yield return new WaitForSeconds(this.startDelay);

            while (true)
            {
                Spawn();
                yield return new WaitForSeconds(this.delay);
                this.delay -= this.stepDelay;

                if (this.delay < this.minDelay) this.delay = this.minDelay;
            }
        }

        private void Spawn()
        {
            Instantiate(this.prefabs[currentPrefab], this.spawns[currentSpawn].position, this.spawns[this.currentSpawn].rotation);
            instances++;
            this.currentSpawn++;

            if (this.currentSpawn >= this.spawns.Length) this.currentSpawn = 0;

            if(prefabs.Length > 1 && instances >= instanceCount[currentPrefab])
            {
                instances = 0;
                currentPrefab++;

                if (currentPrefab >= prefabs.Length) currentPrefab = 0;
            }
        }
    }
}