﻿using System.Collections.Generic;
using UnityEngine;

namespace YsoCorp
{
    public class ParticleVelocity : YCBehaviour
    {
        [SerializeField] private Vector3 velocity;
        private List<ParticleSystem> particles = new List<ParticleSystem>();

        private void Start()
        {
            particles.Add(GetComponent<ParticleSystem>());
            particles.AddRange(GetComponentsInChildren<ParticleSystem>());

            foreach(var p in particles)
            {
                var v = p.velocityOverLifetime;
                v.x = velocity.x;
                v.y = velocity.y;
                v.z = velocity.z;
            }
        }
    }
}