﻿using DG.Tweening;
using DigitalRubyShared;
using System.Collections;
using UnityEngine;

namespace YsoCorp
{
    [RequireComponent(typeof(Rigidbody))]
    public class CarController : YCBehaviour
    {
        [SerializeField] private Transform _forwardPos;
        [SerializeField] private Transform _bottomPos;
        [SerializeField] private float _detectionDistance = 5;
        [SerializeField] private float _groundDetectionDistance = 0.5f;
        [SerializeField] private float _jumpSpeed = 7;
        [SerializeField] private float _hitJumpSpeed = 10;
        [SerializeField] private float _maxHitJumpSpeed = 15;
        [SerializeField] private float _jumpDuration = 0.5f;
        [SerializeField] private float _hitDuration = 0.5f;
        [SerializeField] private float _hitSpeed = 30;
        [SerializeField] private float _hitStep = 1;
        [SerializeField] private float _roadSize = 3;
        [SerializeField] private float _slideSpeed = 100;
        [SerializeField] private Vector3 _hitVolume = new Vector3(2.5f, 1f, 5.5f);
        [SerializeField] private TrailRenderer[] trails;
        [SerializeField] private float _gravityForce = 10;
        [SerializeField] private float _gravityScale = 1;
        [SerializeField] private string animScaleDownId = "ScaleDown";
        [SerializeField] private string animJumpId = "Jump";
        [SerializeField] private float shakePower = 3;
        private Rigidbody _rb;
        private float _jumpTime;
        private float _hitTime;
        private bool _onGround;
        private bool _movingDown;
        private bool _hitCar;
        private float _currentJumpSpeed;
        private float _initHitJumpSpeed;
        private Vector2 _dir;
        private float trailTime;
        private float checkHitRate = 0.5f;
        private float checkHitTime;

        private void Start()
        {
            this._rb = GetComponent<Rigidbody>();
            this._currentJumpSpeed = this._jumpSpeed;

            this.trailTime = this.trails[0].time;
            this._initHitJumpSpeed = this._hitJumpSpeed;

            InitPan();
        }

        private void Update()
        {
            CarDetection();
            DetectGround();

            if(this._movingDown == false)
            {
                Slide();
            }

            //TODO: check if slide down or touch
            if (this._dir.y < -0.2f && this._onGround == false)
            {
                Hit();
            }

            if(this._movingDown == true)
            {
                CheckHit();
            }

            if (this._hitCar == true && this._onGround == true)
            {
                Jump();
            }
        }

        private void FixedUpdate()
        {
            Gravity();
        }

        private void InitPan()
        {
            PanGestureRecognizer pan = new PanGestureRecognizer();
            pan.ThresholdUnits = 0f;
            pan.StateUpdated += (GestureRecognizer gesture) => {
                if (this.game.state != Game.States.Playing)
                {
                    return;
                }
                if (pan.State == GestureRecognizerState.Began)
                {
                }
                else if (pan.State == GestureRecognizerState.Executing)
                {
                    SetSlideDir();
                }
                else if (pan.State == GestureRecognizerState.Ended)
                {
                    ResetSlideDir();
                }
            };
            FingersScript.Instance.AddGesture(pan);
            FingersScript.Instance.ShowTouches = false;
        }

        private void Gravity()
        {
            this._rb.velocity += new Vector3(0, -this._gravityForce * this._gravityScale * Time.fixedDeltaTime, 0);
        }

        private void SetSlideDir()
        {
            this._dir.x = Input.GetAxis("Mouse X");
            this._dir.y = Input.GetAxis("Mouse Y");
        }
        private void ResetSlideDir()
        {
            this._dir = Vector2.zero;
        }

        private void Slide()
        {
            if (Mathf.Abs(transform.position.x) >= this._roadSize)
            {
                if (transform.position.x >= this._roadSize && this._dir.x > 0)
                {
                    this._dir.x = 0;
                }
                else if (transform.position.x <= -this._roadSize && this._dir.x < 0)
                {
                    this._dir.x = 0;
                }
            }

            if (transform.position.x < -this._roadSize) transform.position = new Vector3(-this._roadSize, transform.position.y, transform.position.z);

            if (transform.position.x > this._roadSize) transform.position = new Vector3(this._roadSize, transform.position.y, transform.position.z);

            //Strange lag
            //rb.velocity = new Vector3(dir.x, rb.velocity.y, rb.velocity.z);

            //Looks ok
            transform.position += new Vector3(this._dir.x, 0, 0) * this._slideSpeed * Time.deltaTime;
        }

        private void CarDetection()
        {
            RaycastHit hit;

            if (Physics.Raycast(this._forwardPos.position, this._forwardPos.forward, out hit, this._detectionDistance + 5) && this._onGround)
            {
                if (hit.transform.GetComponent<CarDestruction>() != null)
                {
                    DOTween.Play("ScaleDown");
                }
            }
            else
            {
                DOTween.Rewind("ScaleDown");
            }

            if (Physics.Raycast(this._forwardPos.position, this._forwardPos.forward, out hit, this._detectionDistance) && this._onGround)
            {
                if(hit.transform.GetComponent<CarDestruction>() != null)
                {
                    Jump();
                    DOTween.Rewind("ScaleDown");
                }
            }
        }

        private void Jump()
        {
            if(this._hitCar == false)
            {
                if (Time.time - this._jumpTime < this._jumpDuration) return;
            }

            this._jumpTime = Time.time;

            this._rb.velocity = new Vector3(this._rb.velocity.x, 0, this._rb.velocity.z);
            this._rb.velocity += Vector3.up * this._currentJumpSpeed * _gravityScale;

            this._currentJumpSpeed = this._jumpSpeed;
            this._hitCar = false;

            GameObject vfx = GamePool.Instance.GetObject("vfx_dust_jump");
            GamePool.SetupObject(vfx, transform);

            EnableTrail(true);

            DOTween.Play("Jump");
        }

        private void DetectGround()
        {
            RaycastHit hit;

            if (Physics.Raycast(this._bottomPos.position, Vector3.down, out hit, _groundDetectionDistance))
            {
                //Avoid detecting ground for moving objects (with rigidbody)
                if(this._onGround == false && hit.rigidbody == null)
                {
                    this._onGround = true;
                    this._movingDown = false;

                    EnableTrail(false);

                    DOTween.Rewind("Jump");

                    if(this._hitCar == false)
                    {
                        ResetHitJumpSpeed();
                    }
                }
            }
            else
            {
                this._onGround = false;

                EnableTrail(true);
            }
        }

        private void Hit()
        {
            if (Time.time - this._hitTime < this._hitDuration) return;
            this._hitTime = Time.time;

            MoveDown();
        }

        private void CheckHit()
        {
            if (Time.time - checkHitTime < checkHitRate) return;
            checkHitTime = Time.time;

            RaycastHit[] hits = Physics.BoxCastAll(this._bottomPos.position, _hitVolume / 2, Vector3.down);
            bool carFound = false;

            foreach(var hit in hits)
            {
                if (hit.transform.GetComponent<CarDestruction>() != null)
                {
                    CarDestruction car = hit.transform.GetComponent<CarDestruction>();
                    car.Destroy();
                    carFound = true;
                }
            }

            if(carFound)
            {
                cam.shake.Shake(1 + ((_hitJumpSpeed - _initHitJumpSpeed) / (_maxHitJumpSpeed - _initHitJumpSpeed)) * shakePower);

                this._hitCar = true;
                this._currentJumpSpeed = this._hitJumpSpeed;
                this._hitJumpSpeed += this._hitStep;

                if (this._hitJumpSpeed >= this._maxHitJumpSpeed) this._hitJumpSpeed = this._maxHitJumpSpeed;
            }

            // RaycastHit hit;

            //if (Physics.BoxCastAll(this._bottomPos.position, _hitVolume / 2, Vector3.down, out hit))
            //{
            //    if (hit.transform.GetComponent<CarDestruction>() != null)
            //    {
            //        CarDestruction car = hit.transform.GetComponent<CarDestruction>();
            //        car.Destroy();
            //        this._hitCar = true;
            //        this._currentJumpSpeed = this._hitJumpSpeed;
            //        this._hitJumpSpeed += this._hitStep;

            //        if (this._hitJumpSpeed >= this._maxHitJumpSpeed) this._hitJumpSpeed = this._maxHitJumpSpeed;
            //    }
            //}
        }

        private void MoveDown()
        {
            this._rb.velocity += Vector3.down * this._hitSpeed * _gravityScale;
            this._movingDown = true;
        }

        private void ResetHitJumpSpeed()
        {
            this._hitJumpSpeed = this._initHitJumpSpeed;
        }

        private void EnableTrail(bool enable)
        {
            if (enable)
            {
                foreach (var trail in trails)
                {
                    trail.time = this.trailTime;
                }
            }
            else
            {
                StartCoroutine(TrailFade(1));
            }
        }

        private IEnumerator TrailFade(float duration)
        {
            float t = duration;
            float speed = this.trailTime / duration;

            while(t > 0)
            {
                t -= Time.deltaTime;

                foreach(var trail in trails)
                {
                    trail.time -= speed * Time.deltaTime;
                }

                yield return null;
            }

            foreach (var trail in trails)
            {
                trail.time = 0;
            }
        }

        private void OnDrawGizmos()
        {
            Gizmos.DrawWireCube(this._bottomPos.position, this._hitVolume);
        }
    }
}