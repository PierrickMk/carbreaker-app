﻿using UnityEngine;
using DG.Tweening;
using System.Collections;

namespace YsoCorp
{
    public class CameraShake : YCBehaviour
    {
        [SerializeField] private float _duration = 0.5f;
        [SerializeField] private Vector3 _strength = Vector3.one;
        [SerializeField] private float _randomness = 90;
        [SerializeField] private int _vibrato = 10;
        [SerializeField] private bool _fadeOut = true;
        [SerializeField] private DOTweenAnimation anim;
        private Vector3 _initStrength;
        private Vector3 initPos;
        private bool running;

        private void Start()
        {
            _initStrength = anim.endValueV3;
        }

        public void Shake(float power = 1)
        {
            //Debug.Log(power);
            //anim.endValueV3 = _initStrength * power * 5;
            //anim.DORewind();
            //anim.DOPlay();

            this.transform.DOShakePosition(_duration, _strength * power, _vibrato, _randomness, false, _fadeOut);

            //StartCoroutine(ShakeProcess(power));
        }

        //private IEnumerator ShakeProcess(float power)
        //{
        //    if(running) yield return new WaitForEndOfFrame();

        //    running = true;
        //    this.transform.DOShakePosition(_duration, _strength/* * power*/, _vibrato, _randomness, false, _fadeOut);

        //    yield return new WaitForSeconds(_duration);
        //    running = false;

        //    float t = 1;

        //    while(t > 0)
        //    {
        //        if (running == true) yield break;

        //        t -= Time.deltaTime;
        //        transform.position = Vector3.MoveTowards(transform.position, initPos, 10 * Time.deltaTime);
        //        yield return null;
        //    }
        //}
    }
}